;; GNU/Emacs custom init file
;; ---
;; Writen by Thomas Piekarski <piekarski@member.fsf.org>
;;
;; Resources:
;;   - https://lucidmanager.org/productivity/configure-emacs/
;;   - https://www.emacswiki.org/
;;   - https://github.com/Fanael/init.el
;;

;; Start Emacs Server
(server-start)

;; Define and initialize package repositories
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

;; Use use-package to simplify the config file
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; Ensure that any missing package will be automatically installed
(require 'use-package)
(setq use-package-always-ensure 't)

;; Set window title, display and size
(setq icon-title-format
      (setq frame-title-format "%b (%f) - GNU/Emacs"))
(setq-default indicate-empty-lines t)
(setq-default indicate-buffer-boundaries 'left)
(setq default-frame-alist '((width . 160) (height . 40)))

;; Load custom theme
(use-package simplicity-theme
  :config (load-theme 'simplicity t))

;; Set custom font
(let ((font-family "DejaVu Sans Mono"))
  (when (or(eq system-type 'windows-nt) (eq system-type 'cygwin))
    (setf font-family "Consolas"))
  (set-face-attribute 'default nil
                      :family font-family
                      :height 120
                      :weight 'normal
                      :width 'normal))

;; Remove any distraction and center on a keyboard-driven interface
(setq inhibit-startup-message t)
(setq initial-scratch-message "")
(setq use-dialog-box nil)
(setq use-file-dialog nil)
(setq visible-bell 1)
(setq-default cursor-type 'bar)
(blink-cursor-mode 0)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(set-cursor-color "#8b0000")
(tool-bar-mode -1)

;; Smooth and less 'jumpy' scrolling
(setq mouse-wheel-follow-mouse 't)
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq scroll-step 1)

;; Indention
(setq-default indent-tabs-mode nil)
(defvaralias 'c-basic-offset 'tab-width)
(setq-default tab-width 4)

;; Disable backup files, enable auto-revert for external changes and start using git
(setq make-backup-files nil)
(global-auto-revert-mode 1)

;; Increase undo limits
(setq undo-limit (* 8 1024 1024))
(setq undo-strong-limit (* 16 1024 1024))
(setq undo-outer-limit (* 32 1024 1024))

;; History
(require 'savehist)
(setq history-length 1024)
(setq history-delete-duplicates t)
(setq search-ring-max 1024)
(setq regexp-search-ring-max 1024)
(setq savehist-additional-variables '(extended-command-history file-name-history search-ring regexp-search-ring))
(setq savehist-file (expand-file-name "savehist" user-emacs-directory))
(savehist-mode)

;; Highlight search and replace
(setq search-highlight t)
(setq query-replace-highlight t)

;; Force UTF-8
(prefer-coding-system 'utf-8)
(setq locale-coding-system 'utf-8)
(setq-default buffer-file-coding-system 'utf-8-unix)
(set-buffer-file-coding-system 'utf-8)
(set-clipboard-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-file-name-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-language-environment "UTF-8")
(set-selection-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)

;;; Move auto-save files to users emacs directory
(let ((auto-save-dir (file-name-as-directory (expand-file-name "autosave" user-emacs-directory))))
  (setq auto-save-list-file-prefix (expand-file-name ".saves-" auto-save-dir))
  (setq auto-save-file-name-transforms (list (list ".*" (replace-quote auto-save-dir) t))))

;; neotree
(use-package neotree)
(require 'neotree)
(global-set-key [f8] 'neotree-toggle)
(setq neo-smart-open t)
(setq neo-window-fixed-size nil)
(setq neo-window-width 60)

;; Trick neotree into resetting the width back to the actual window width.
;; ---
;; https://github.com/jaypei/emacs-neotree/issues/262
(eval-after-load "neotree"
  '(add-to-list 'window-size-change-functions
                (lambda (frame)
                  (let ((neo-window (neo-global--get-window)))
                    (unless (null neo-window)
                      (setq neo-window-width (window-width neo-window)))))))


;; SLIME - Common Lisp for Emacs
(when (string= (system-name) "tara")
  (load (expand-file-name "~/.quicklisp/slime-helper.el"))
  (setq inferior-lisp-program "sbcl"))

;; Markdown major mode
(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))
