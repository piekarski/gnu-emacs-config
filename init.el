;; GNU/Emacs default init file
;; ---
;; Writen by GNU/Emacs and Thomas Piekarski <piekarski@member.fsf.org>
;;

;; Define a custom init file
(setq custom-init-file (expand-file-name "custom-init.el" user-emacs-directory))
(when (file-exists-p custom-init-file)
  (load custom-init-file))

;; GNU/Emacs init part...
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(emacs-simplicity-theme simplicity simplicity-theme markdown-mode cyberpunk-theme use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
